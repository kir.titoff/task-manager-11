package ru.t1.ktitov.tm.api;

public interface ICommandController {

    void showInfo();

    void showErrorArgument(String arg);

    void showErrorCommand();

    void showErrorCommand(String command);

    void showVersion();

    void showAbout();

    void showArguments();

    void showCommands();

    void showHelp();

}
