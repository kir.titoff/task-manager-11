package ru.t1.ktitov.tm.api;

public interface IProjectController {

    void createProject();

    void clearProjects();

    void showProjects();

    void showProjectById();

    void showProjectByIndex();

    void removeProjectById();

    void removeProjectByIndex();

    void updateProjectById();

    void updateProjectByIndex();

}
